//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "ScavTrap.hpp"


ScavTrap::ScavTrap( std::string name ) {
	srand(time(NULL));
	this->hit_points = 100;
	this->max_hit_points = 100;
	this->energy_points = 50;
	this->max_energy_points = 50;
	this->level = 1;
	this->melee_attack_damage = 20;
	this->ranged_attack_damage = 15;
	this->armor_damage_reduction = 3;
	this->name = name;
	std::cout << "[SC4W-TP " << this->name << " was created] Glitching weirdness is a term of endearment, right?" << std::endl;

	return ;
}

ScavTrap::~ScavTrap() {
	std::cout << "[SC4W-TP " << this->name << " was destroyed] Argh arghargh death gurgle gurglegurgle urgh... death." << std::endl;

	return ;
}

ScavTrap::ScavTrap(const ScavTrap &old) {
	*this = old;

	return ;
}

void ScavTrap::operator=(const ScavTrap &rhs) {
	this->armor_damage_reduction = rhs.getArmor_damage_reduction();
	this->energy_points = rhs.getEnergy_points();
	this->hit_points = rhs.getHit_points();
	this->level = rhs.getLevel();
	this->max_energy_points = rhs.getMax_energy_points();
	this->max_hit_points = rhs.getMax_hit_points();
	this->melee_attack_damage = rhs.getMelee_attack_damage();
	this->ranged_attack_damage = rhs.getRanged_attack_damage();
	this->name = rhs.getName();

	return ;
}

//----------------------------------getters-----------------------------------//

int ScavTrap::getHit_points() const { return hit_points; }

int ScavTrap::getMax_hit_points() const { return max_hit_points; }

int ScavTrap::getEnergy_points() const { return energy_points; }

int ScavTrap::getMax_energy_points() const { return max_energy_points; }

int ScavTrap::getLevel() const { return level; }

int ScavTrap::getMelee_attack_damage() const { return melee_attack_damage; }

int ScavTrap::getRanged_attack_damage() const { return ranged_attack_damage; }

int ScavTrap::getArmor_damage_reduction() const { return armor_damage_reduction; }

const std::string &ScavTrap::getName() const { return name; }

//------------------------------general actions--------------------------------//

void ScavTrap::rangedAttack( std::string const & target ) {
	int damage;

	damage = this->ranged_attack_damage;
	this->print_action("Ranged attack");

	std::cout << "Fires with his weapon into "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void ScavTrap::meleeAttack( std::string const & target ) {
	int damage;

	damage = this->melee_attack_damage;
	this->print_action("Melee attack");

	std::cout << "Rushes and kicks "
			  << target << " which deals "
			  << damage << " damage" << std::endl;

	return ;
}

void ScavTrap::takeDamage( unsigned int amount ) {
	this->hit_points -= (amount - this->armor_damage_reduction);

	std::cout << "[SC4W-TP " << this->name << " takes " << amount << " damage]";
	if (this->hit_points > 0)
	{
		std::cout << "My health is decreased to "
				  << this->hit_points << " point " << std::endl;
	}
	else
	{
		this->hit_points = 0;
		std::cout << "Looks like I can't handle this anymore " << std::endl;
	}

	return ;
}

void ScavTrap::beRepaired( unsigned int amount ) {
	this->hit_points += amount;

	std::cout << "[SC4W-TP " << this->name << " repaired " << amount << " point]";
	this->hit_points = (this->hit_points > this->max_hit_points ? this->max_hit_points : this->hit_points);
	std::cout << "It's much better now" << std::endl;
}

void ScavTrap::challengeNewcomer( std::string const & target ) {
	void (ScavTrap::*perform_actions[])(std::string const&) = {&ScavTrap::challenge_infinity, &ScavTrap::challenge_piscine, &ScavTrap::challenge_war_and_peace, &ScavTrap::challenge_story, &ScavTrap::challenge_bear_grills};
	int operation = this->getRandomValue(1, 5);

	if (this->energy_points > 25)
	{
		(this->*perform_actions[operation])(target);
		this->energy_points -= 25;
	}
	else
		std::cout << "[SC4W-TP " << this->name << " wanted to perform Vaulthunter.exe but is he run out of energy]" << std::endl;
}

//---------------------------vaulthhunter actions-----------------------------//

void ScavTrap::challenge_infinity( std::string const & target ) {

	std::cout << "Hey "<< target << ", I have a nice challenge for you. I bet you can't count to infinity, aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_piscine( std::string const & target ) {
	std::cout << "Howdy "<< target << ", I have a glorious challenge for you. I bet you can't pass the piscine C in school 42, aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_war_and_peace( std::string const & target ) {
	std::cout << "Howdy "<< target << ". I have an exhaustive challenge for you. I bet my whole metal liver that you won't read \"War and Peace\", aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_story( std::string const & target ) {
	std::cout << "Hello "<< target << ". I am tired of those crappy challenges. Bring me a cup of tea, this will be enough for now." << std::endl;

	return ;
}

void ScavTrap::challenge_bear_grills( std::string const & target ) {
	std::cout << "Cheers "<< target << ". I have an ultimate challenge. You must do everything that Bear Grills done in his crazy TV series on Discovery. " << std::endl;
	return ;
}

void ScavTrap::print_action( std::string const & action ) {
	std::cout << "[SC4W-TP " << this->name << " performs " << action << "] ";
}

int ScavTrap::getRandomValue(int floor, int ceiling) {
	int randNum = rand()%(ceiling - floor + 1) + floor;
	randNum = randNum % (ceiling - floor) + floor;

	return (randNum);
}