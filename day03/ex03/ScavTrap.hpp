//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_SCAVTRAP_HPP
#define PISCINE_CPP_SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap: public ClapTrap
{
public:
	ScavTrap( );
	ScavTrap( std::string name );
	ScavTrap( const ScavTrap& );

	~ScavTrap();

	void challengeNewcomer( std::string const & target );

	static int getRandomValue( int floor, int ceiling );

private:

	void challenge_infinity( std::string const & target );
	void challenge_piscine( std::string const & target );
	void challenge_war_and_peace( std::string const & target );
	void challenge_story( std::string const & target );
	void challenge_bear_grills( std::string const & target );

};


#endif //PISCINE_CPP_SCAVTRAP_HPP
