//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_NINJA_HPP
#define PISCINE_CPP_NINJA_HPP

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

class NinjaTrap : public ClapTrap
{
public:
	NinjaTrap();
	NinjaTrap(std::string name);

	~NinjaTrap();

	void ninjaShoebox( ClapTrap & obj );
	void ninjaShoebox( ScavTrap & obj );
	void ninjaShoebox( FragTrap & obj );
	void ninjaShoebox( NinjaTrap & obj);
};


#endif //PISCINE_CPP_NINJA_HPP
