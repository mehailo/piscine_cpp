//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap() : ClapTrap()
{
	this->type = "NINJ4-TP";
	std::cout << "NinjaTrap constructed" << std::endl;

	return ;
}

NinjaTrap::NinjaTrap(std::string name) : ClapTrap (name)
{
	this->hit_points = 60;
	this->max_hit_points = 60;
	this->armor_damage_reduction = 0;
	this->energy_points = 120;
	this->max_energy_points = 120;
	this->level = 1;
	this->melee_attack_damage = 60;
	this->ranged_attack_damage = 5;
	this->type = "NINJ4-TP";
	std::cout << "NinjaTrap constructed" << std::endl;
	return ;
}

NinjaTrap::~NinjaTrap()
{
	std::cout << "NinjaTrap destructed" << std::endl;
}

void NinjaTrap::ninjaShoebox( ClapTrap & obj )
{
	obj.meleeAttack("Stranger Clap");
	obj.rangedAttack("Stranger Clap");
}

void NinjaTrap::ninjaShoebox( FragTrap & obj )
{
	obj.meleeAttack("Stranger Frag");
	obj.rangedAttack("Stranger Frag");
}

void NinjaTrap::ninjaShoebox( ScavTrap & obj )
{
	obj.meleeAttack("Stranger Scav");
	obj.rangedAttack("Stranger Scav");
}

void NinjaTrap::ninjaShoebox( NinjaTrap & obj )
{
	obj.meleeAttack("Stranger Ninja");
	obj.rangedAttack("Stranger Ninja");
}
