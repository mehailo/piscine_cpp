//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "ClapTrap.hpp"

ClapTrap::ClapTrap()
{
	ClapTrap obj("CL4P-TP");
	*this = obj;
	return ;
}

ClapTrap::ClapTrap( const ClapTrap &old ) {
	*this = old;

	return ;
}

ClapTrap::~ClapTrap() {
	std::cout << "[" << this->type <<  " " << this->name << " dies]" << std::endl;
}

ClapTrap::ClapTrap( std::string name ) {
	srand(time(NULL));
	this->hit_points = 50;
	this->max_hit_points = 50;
	this->energy_points = 50;
	this->max_energy_points = 50;
	this->level = 1;
	this->melee_attack_damage = 15;
	this->ranged_attack_damage = 10;
	this->armor_damage_reduction = 2;
	this->name = name;
	this->type = "CL4P-TP";
	std::cout << "[" << this->name << " was created] Glitching weirdness is a term of endearment, right?" << std::endl;

	return ;
}

void ClapTrap::operator=(const ClapTrap &rhs) {
	this->armor_damage_reduction = rhs.getArmor_damage_reduction();
	this->energy_points = rhs.getEnergy_points();
	this->hit_points = rhs.getHit_points();
	this->level = rhs.getLevel();
	this->max_energy_points = rhs.getMax_energy_points();
	this->max_hit_points = rhs.getMax_hit_points();
	this->melee_attack_damage = rhs.getMelee_attack_damage();
	this->ranged_attack_damage = rhs.getRanged_attack_damage();
	this->name = rhs.getName();
	this->type = rhs.getType();


	return ;
}

//----------------------------------getters-----------------------------------//

int ClapTrap::getHit_points() const { return hit_points; }

int ClapTrap::getMax_hit_points() const { return max_hit_points; }

int ClapTrap::getEnergy_points() const { return energy_points; }

int ClapTrap::getMax_energy_points() const { return max_energy_points; }

int ClapTrap::getLevel() const { return level; }

int ClapTrap::getMelee_attack_damage() const { return melee_attack_damage; }

int ClapTrap::getRanged_attack_damage() const { return ranged_attack_damage; }

int ClapTrap::getArmor_damage_reduction() const { return armor_damage_reduction; }

const std::string &ClapTrap::getName() const { return name; }

const std::string &ClapTrap::getType() const { return type; }

//------------------------------general actions-------------------------------//

void ClapTrap::rangedAttack( std::string const & target ) {
	int damage;

	damage = this->ranged_attack_damage;
	this->print_action("Ranged attack");

	std::cout << "Fires with his weapon into "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void ClapTrap::meleeAttack( std::string const & target ) {
	int damage;

	damage = this->melee_attack_damage;
	this->print_action("Melee attack");

	std::cout << "Rushes and kicks "
			  << target << " which deals "
			  << damage << " damage" << std::endl;

	return ;
}

void ClapTrap::takeDamage( unsigned int amount ) {
	this->hit_points -= (amount - this->armor_damage_reduction);

	std::cout << "[" << this->type << " " << this->name << " takes " << amount << " damage]";
	if (this->hit_points > 0)
	{
		std::cout << "My health is decreased to "
				  << this->hit_points << " point " << std::endl;
	}
	else
	{
		this->hit_points = 0;
		std::cout << "Looks like I can't handle this anymore " << std::endl;
	}

	return ;
}

void ClapTrap::beRepaired( unsigned int amount ) {
	this->hit_points += amount;

	std::cout << "[" << this->type << " " << this->name << " repaired " << amount << " point]";
	this->hit_points = (this->hit_points > this->max_hit_points ? this->max_hit_points : this->hit_points);
	std::cout << "It's much better now" << std::endl;
}

//------------------------help functions--------------------------------------//

void ClapTrap::print_action( std::string const & action ) {
	std::cout << "[" << this->type << " " << this->name << " performs " << action << "] ";
}

int ClapTrap::getRandomValue(int floor, int ceiling) {
	int randNum = rand()%(ceiling - floor + 1) + floor;
	randNum = randNum % (ceiling - floor) + floor;

	return (randNum);
}