//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_FRAGTRAP_HPP
#define PISCINE_CPP_FRAGTRAP_HPP

#include <string>
#include <iostream>
#include <random>
#include <ctime>
#include "ClapTrap.hpp"

class FragTrap: public ClapTrap
{
public:
	FragTrap( ) : ClapTrap() {this->type = "FR4G-TP";
		std::cout << "Fragtrap constructed" << std::endl;};
	FragTrap(std::string name) : ClapTrap(name) {this->type = "FR4G-TP";
		std::cout << "Fragtrap constructed" << std::endl;};
	FragTrap( const FragTrap& );

	~FragTrap();

	void operator=( const ClapTrap& rhs );

	void vaulthunter_dot_exe( std::string const & target );

private:

	void vh_clap_in_the_box( std::string const & target );
	void vh_one_shot_wonder( std::string const & target );
	void vh_funzerker( std::string const & target );
	void vh_mechromagician( std::string const & target );
	void vh_medbot( std::string const & target );

};

#endif //PISCINE_CPP_FRAGTRAP_HPP
