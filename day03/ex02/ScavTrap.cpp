//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "ScavTrap.hpp"

ScavTrap::~ScavTrap() {
	std::cout << "Scavtrap destroyed" << std::endl;

	return ;
}

void ScavTrap::operator=(const ScavTrap &rhs) {
	this->armor_damage_reduction = rhs.getArmor_damage_reduction();
	this->energy_points = rhs.getEnergy_points();
	this->hit_points = rhs.getHit_points();
	this->level = rhs.getLevel();
	this->max_energy_points = rhs.getMax_energy_points();
	this->max_hit_points = rhs.getMax_hit_points();
	this->melee_attack_damage = rhs.getMelee_attack_damage();
	this->ranged_attack_damage = rhs.getRanged_attack_damage();
	this->name = rhs.getName();

	return ;
}

//--------------------------general actions-----------------------------------//

void ScavTrap::challengeNewcomer( std::string const & target ) {
	void (ScavTrap::*perform_actions[])(std::string const&) = {&ScavTrap::challenge_infinity, &ScavTrap::challenge_piscine, &ScavTrap::challenge_war_and_peace, &ScavTrap::challenge_story, &ScavTrap::challenge_bear_grills};
	int operation = this->getRandomValue(1, 5);

	if (this->energy_points > 25)
	{
		(this->*perform_actions[operation])(target);
		this->energy_points -= 25;
	}
	else
		std::cout << "[SC4W-TP " << this->name << " wanted to perform Vaulthunter.exe but is he run out of energy]" << std::endl;
}

//---------------------------vaulthhunter actions-----------------------------//

void ScavTrap::challenge_infinity( std::string const & target ) {

	std::cout << "Hey "<< target << ", I have a nice challenge for you. I bet you can't count to infinity, aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_piscine( std::string const & target ) {
	std::cout << "Howdy "<< target << ", I have a glorious challenge for you. I bet you can't pass the piscine C in school 42, aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_war_and_peace( std::string const & target ) {
	std::cout << "Howdy "<< target << ". I have an exhaustive challenge for you. I bet my whole metal liver that you won't read \"War and Peace\", aren't you?" << std::endl;

	return ;
}

void ScavTrap::challenge_story( std::string const & target ) {
	std::cout << "Hello "<< target << ". I am tired of those crappy challenges. Bring me a cup of tea, this will be enough for now." << std::endl;

	return ;
}

void ScavTrap::challenge_bear_grills( std::string const & target ) {
	std::cout << "Cheers "<< target << ". I have an ultimate challenge. You must do everything that Bear Grills done in his crazy TV series on Discovery. " << std::endl;
	return ;
}

int ScavTrap::getRandomValue(int floor, int ceiling) {
	int randNum = rand()%(ceiling - floor + 1) + floor;
	randNum = randNum % (ceiling - floor) + floor;

	return (randNum);
}