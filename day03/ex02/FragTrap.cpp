//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "FragTrap.hpp"


void FragTrap::operator=(const ClapTrap &rhs) {
	this->armor_damage_reduction = rhs.getArmor_damage_reduction();
	this->energy_points = rhs.getEnergy_points();
	this->hit_points = rhs.getHit_points();
	this->level = rhs.getLevel();
	this->max_energy_points = rhs.getMax_energy_points();
	this->max_hit_points = rhs.getMax_hit_points();
	this->melee_attack_damage = rhs.getMelee_attack_damage();
	this->ranged_attack_damage = rhs.getRanged_attack_damage();
	this->name = rhs.getName();

	return ;
}

FragTrap::~FragTrap() {
	std::cout << "Fragtrap destructed" << std::endl;

	return ;
}

void FragTrap::vaulthunter_dot_exe( std::string const & target ) {
	void (FragTrap::*perform_actions[])(std::string const&) = {&FragTrap::vh_clap_in_the_box, &FragTrap::vh_one_shot_wonder, &FragTrap::vh_funzerker, &FragTrap::vh_mechromagician, &FragTrap::vh_medbot};
	int operation = this->getRandomValue(1, 5);

	if (this->energy_points > 25)
	{
		(this->*perform_actions[operation])(target);
		this->energy_points -= 25;
	}
	else
		std::cout << "[FR4G-TP " << this->name << " wanted to perform Vaulthunter.exe but is he run out of energy]" << std::endl;
}

//---------------------------vaulthhunter actions-----------------------------//

void FragTrap::vh_clap_in_the_box( std::string const &target ) {
	int damage = FragTrap::getRandomValue(30, 40);

	this->print_action("Clap in the box");
	std::cout << "Strikes directly in the head of "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::vh_one_shot_wonder( std::string const & target ) {
	int damage = FragTrap::getRandomValue(25, 35);

	this->print_action("One shot wonder");
	std::cout << "Throwing massive object in "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::vh_funzerker( std::string const & target ) {
	int damage = FragTrap::getRandomValue(5, 50);

	this->print_action("Funkerzer");
	std::cout << "Pours a bit of oil on "
			  << target;

	if (damage > 15)
	{
		std::cout << " and strikes with a flamethrower which cause ";
		if (damage > 40)
			std::cout << "an abollishing " << damage << " amound of damage" << std::endl;
		else
			std::cout << damage << " amound of damage" << std::endl;
	}
	else
	{
		std::cout << " and misses with a flamethrower which cause only "
				  << damage << " damage" << std::endl;
	}
	return ;
}

void FragTrap::vh_mechromagician( std::string const & target ) {
	int damage = FragTrap::getRandomValue(20, 30);

	this->print_action("Medbot");
	std::cout << "Swings with arms dealing to "
			  << target << " " << damage << " point of damage" << std::endl;

	return ;
}

void FragTrap::vh_medbot( std::string const & target ) {
	unsigned int heal = (unsigned)FragTrap::getRandomValue(5, 10);

	this->print_action("Medbot");
	std::cout << "performing ranged attack and heal himself " << std::endl;
	this->rangedAttack(target);
	this->beRepaired(heal);
	return ;
}