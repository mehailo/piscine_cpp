//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_SCAVTRAP_HPP
#define PISCINE_CPP_SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap: public ClapTrap
{
public:
	ScavTrap( ) : ClapTrap() {this->type = "FR4G-TP";
		std::cout << "Scavtrap constructed" << std::endl;};
	ScavTrap(std::string name) : ClapTrap(name) {this->type = "SC4V-TP";
	std::cout << "Scavtrap constructed" << std::endl;};
	ScavTrap( const ScavTrap& );

	~ScavTrap();

	void operator=( const ScavTrap& rhs );

	void challengeNewcomer( std::string const & target );

	static int getRandomValue( int floor, int ceiling );

private:

	void challenge_infinity( std::string const & target );
	void challenge_piscine( std::string const & target );
	void challenge_war_and_peace( std::string const & target );
	void challenge_story( std::string const & target );
	void challenge_bear_grills( std::string const & target );

};


#endif //PISCINE_CPP_SCAVTRAP_HPP
