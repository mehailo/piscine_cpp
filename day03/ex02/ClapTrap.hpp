//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_CLAPTRAP_HPP
#define PISCINE_CPP_CLAPTRAP_HPP

#include <string>
#include <iostream>
#include <random>
#include <ctime>

class ClapTrap
{
public:
	ClapTrap();
	ClapTrap( std::string name );
	ClapTrap( const ClapTrap &old );

	~ClapTrap();

	int getHit_points() const;
	int getMax_hit_points() const;
	int getEnergy_points() const;
	int getMax_energy_points() const;
	int getLevel() const;
	int getMelee_attack_damage() const;
	int getRanged_attack_damage() const;
	int getArmor_damage_reduction() const;
	const std::string &getName() const;

	void rangedAttack( std::string const & target );
	void meleeAttack( std::string const & target );
	void takeDamage( unsigned int amount );
	void beRepaired( unsigned int amount );

	static int getRandomValue( int floor, int ceiling );

protected:
	int hit_points;
	int max_hit_points;
	int energy_points;
	int max_energy_points;
	int level;
	int melee_attack_damage;
	int ranged_attack_damage;
	int armor_damage_reduction;
	std::string type;
	std::string name;

	void print_action( std::string const & action );
};


#endif //PISCINE_CPP_CLAPTRAP_HPP
