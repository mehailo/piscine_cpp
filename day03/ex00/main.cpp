//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "FragTrap.hpp"

int main()
{
	FragTrap bot("Manny");

	bot.rangedAttack("bunny");
	bot.vaulthunter_dot_exe("bunny");
	bot.rangedAttack("bunny");
	bot.rangedAttack("bunny");
	bot.rangedAttack("bunny");
	bot.vaulthunter_dot_exe("bunny");
	bot.vaulthunter_dot_exe("bunny");
	bot.vaulthunter_dot_exe("bunny");
	bot.takeDamage(104);
	bot.vaulthunter_dot_exe("bunny");

	return (0);
}

