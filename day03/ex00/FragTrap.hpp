//
// Created by Mykhailo Frankevich on 5/8/17.
//

#ifndef PISCINE_CPP_FRAGTRAP_HPP
#define PISCINE_CPP_FRAGTRAP_HPP

#include <string>
#include <iostream>
#include <random>
#include <ctime>

class FragTrap
{
public:
	FragTrap( std::string name );
	FragTrap( const FragTrap& );

	~FragTrap();

	void operator=( const FragTrap& rhs );

	int getHit_points() const;
	int getMax_hit_points() const;
	int getEnergy_points() const;
	int getMax_energy_points() const;
	int getLevel() const;
	int getMelee_attack_damage() const;
	int getRanged_attack_damage() const;
	int getArmor_damage_reduction() const;
	const std::string &getName() const;

	void rangedAttack( std::string const & target );
	void meleeAttack( std::string const & target );
	void takeDamage( unsigned int amount );
	void beRepaired( unsigned int amount );
	void vaulthunter_dot_exe( std::string const & target );

	static int getRandomValue( int floor, int ceiling );

private:
	int hit_points;
	int max_hit_points;
	int energy_points;
	int max_energy_points;
	int level;
	int melee_attack_damage;
	int ranged_attack_damage;
	int armor_damage_reduction;
	std::string name;

	void vh_clap_in_the_box( std::string const & target );
	void vh_one_shot_wonder( std::string const & target );
	void vh_funzerker( std::string const & target );
	void vh_mechromagician( std::string const & target );
	void vh_medbot( std::string const & target );

	void print_action( std::string const & action );

};

#endif //PISCINE_CPP_FRAGTRAP_HPP
