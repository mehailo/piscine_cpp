//
// Created by Mykhailo Frankevich on 5/8/17.
//

#include "FragTrap.hpp"

FragTrap::FragTrap( std::string name ) {
	srand(time(NULL));
	this->hit_points = 100;
	this->max_hit_points = 100;
	this->energy_points = 100;
	this->max_energy_points = 100;
	this->level = 1;
	this->melee_attack_damage = 30;
	this->ranged_attack_damage = 20;
	this->armor_damage_reduction = 5;
	this->name = name;
	std::cout << "[FR4G-TP " << this->name << " was created] Glitching weirdness is a term of endearment, right?" << std::endl;

	return ;
}

FragTrap::~FragTrap() {
	std::cout << "[FR4G-TP " << this->name << " was destroyed] Argh arghargh death gurgle gurglegurgle urgh... death." << std::endl;

	return ;
}

FragTrap::FragTrap(const FragTrap &old) {
	*this = old;

	return ;
}

void FragTrap::operator=(const FragTrap &rhs) {
	this->armor_damage_reduction = rhs.getArmor_damage_reduction();
	this->energy_points = rhs.getEnergy_points();
	this->hit_points = rhs.getHit_points();
	this->level = rhs.getLevel();
	this->max_energy_points = rhs.getMax_energy_points();
	this->max_hit_points = rhs.getMax_hit_points();
	this->melee_attack_damage = rhs.getMelee_attack_damage();
	this->ranged_attack_damage = rhs.getRanged_attack_damage();
	this->name = rhs.getName();

	return ;
}

//----------------------------------getters-----------------------------------//

int FragTrap::getHit_points() const { return hit_points; }

int FragTrap::getMax_hit_points() const { return max_hit_points; }

int FragTrap::getEnergy_points() const { return energy_points; }

int FragTrap::getMax_energy_points() const { return max_energy_points; }

int FragTrap::getLevel() const { return level; }

int FragTrap::getMelee_attack_damage() const { return melee_attack_damage; }

int FragTrap::getRanged_attack_damage() const { return ranged_attack_damage; }

int FragTrap::getArmor_damage_reduction() const { return armor_damage_reduction; }

const std::string &FragTrap::getName() const { return name; }

//------------------------------general actions-------------------------------//

void FragTrap::rangedAttack( std::string const & target ) {
	int damage;

	damage = this->ranged_attack_damage;
	this->print_action("Ranged attack");

	std::cout << "Fires with his weapon into "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::meleeAttack( std::string const & target ) {
	int damage;

	damage = this->melee_attack_damage;
	this->print_action("Melee attack");

	std::cout << "Rushes and kicks "
			  << target << " which deals "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::takeDamage( unsigned int amount ) {
	this->hit_points -= (amount - this->armor_damage_reduction);

	std::cout << "[FR4G-TP " << this->name << " takes " << amount << " damage]";
	if (this->hit_points > 0)
	{
		std::cout << "My health is decreased to "
				  << this->hit_points << " point " << std::endl;
	}
	else
	{
		this->hit_points = 0;
		std::cout << "Looks like I can't handle this anymore " << std::endl;
	}

	return ;
}

void FragTrap::beRepaired( unsigned int amount ) {
	this->hit_points += amount;

	std::cout << "[FR4G-TP " << this->name << " repaired " << amount << " point]";
	this->hit_points = (this->hit_points > this->max_hit_points ? this->max_hit_points : this->hit_points);
	std::cout << "It's much better now" << std::endl;
}

void FragTrap::vaulthunter_dot_exe( std::string const & target ) {
	void (FragTrap::*perform_actions[])(std::string const&) = {&FragTrap::vh_clap_in_the_box, &FragTrap::vh_one_shot_wonder, &FragTrap::vh_funzerker, &FragTrap::vh_mechromagician, &FragTrap::vh_medbot};
	int operation = this->getRandomValue(1, 5);

	if (this->energy_points > 25)
	{
		(this->*perform_actions[operation])(target);
		this->energy_points -= 25;
	}
	else
		std::cout << "[FR4G-TP " << this->name << " wanted to perform Vaulthunter.exe but is he run out of energy]" << std::endl;
}

//---------------------------vaulthhunter actions-----------------------------//

void FragTrap::vh_clap_in_the_box( std::string const &target ) {
	int damage = FragTrap::getRandomValue(30, 40);

	this->print_action("Clap in the box");
	std::cout << "Strikes directly in the head of "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::vh_one_shot_wonder( std::string const & target ) {
	int damage = FragTrap::getRandomValue(25, 35);

	this->print_action("One shot wonder");
	std::cout << "Throwing massive object in "
			  << target << " and causing "
			  << damage << " damage" << std::endl;

	return ;
}

void FragTrap::vh_funzerker( std::string const & target ) {
	int damage = FragTrap::getRandomValue(5, 50);

	this->print_action("Funkerzer");
	std::cout << "Pours a bit of oil on "
			  << target;

	if (damage > 15)
	{
		std::cout << " and strikes with a flamethrower which cause ";
		if (damage > 40)
			std::cout << "an abollishing " << damage << " amound of damage" << std::endl;
		else
			std::cout << damage << " amound of damage" << std::endl;
	}
	else
	{
		std::cout << " and misses with a flamethrower which cause only "
				  << damage << " damage" << std::endl;
	}
	return ;
}

void FragTrap::vh_mechromagician( std::string const & target ) {
	int damage = FragTrap::getRandomValue(20, 30);

	this->print_action("Medbot");
	std::cout << "Swings with arms dealing to "
			  << target << " " << damage << " point of damage" << std::endl;

	return ;
}

void FragTrap::vh_medbot( std::string const & target ) {
	unsigned int heal = (unsigned)FragTrap::getRandomValue(5, 10);

	this->print_action("Medbot");
	std::cout << "performing ranged attack and heal himself " << std::endl;
	this->rangedAttack(target);
	this->beRepaired(heal);
	return ;
}

void FragTrap::print_action( std::string const & action ) {
	std::cout << "[FR4G-TP " << this->name << " performs " << action << "] ";
}

int FragTrap::getRandomValue(int floor, int ceiling) {
	int randNum = rand()%(ceiling - floor + 1) + floor;
	randNum = randNum % (ceiling - floor) + floor;

	return (randNum);
}