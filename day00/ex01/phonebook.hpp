#pragma once
#include <string>
#include <iostream>
#include <iomanip>

class PhoneBook
{
public:
	PhoneBook();
	virtual ~PhoneBook();

	void AddContact();
	void SearchContact();
	void ClosePhoneBook();

private:
	void _ShowContactShort(int i_index);
	void _ShowContactFull(int i_index);
	void _PrintField(std::string i_str);
	void _GetLineFromInput(std::string &i_str);
	bool  _GetNumberFromInput(int &o_num);

private:
	int m_contacts_count;
	struct contact { std::string field[11]; } m_contacts[8];
};