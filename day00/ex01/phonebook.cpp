#include "phonebook.hpp"

PhoneBook::PhoneBook() : m_contacts_count(0)
{
	return ;
}

PhoneBook::~PhoneBook()
{
  return ;
}

void PhoneBook::_GetLineFromInput(std::string &i_str)
{
  if (!std::getline(std::cin, i_str))
    exit(1);
}

bool PhoneBook::_GetNumberFromInput(int &o_num)
{
  std::string line;
  _GetLineFromInput(line);
 
  int num = atoi(line.c_str());
  if (num == 0 || line == "0")
    return false;
  o_num = num;
  return true;
}

void		PhoneBook::_PrintField(std::string str)
{
	if (str.length() > 10)
	{
		std::cout << std::setw(10);
		std::cout << str.substr(0, 9) << ".";
	}
	else
	{
		std::cout << std::setw(10);
		std::cout << str;
	}
	return ;
}

void PhoneBook::AddContact()
{
	std::string fields_titles[] = {
		"first name", 
		"last name", 
		"nickname", 
		"login", 
		"postal address", 
		"email address", 
		"phone number", 
		"birthday date", 
		"favorite meal", 
		"underwear color" , 
		"darkest secret"};
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  std::cin.clear();
  for (int j = 0; j < 11; ++j)
	{
		std::cout << fields_titles[j] << ": ";
    _GetLineFromInput(m_contacts[m_contacts_count].field[j]);
    if (m_contacts[m_contacts_count].field[j].length() == 0)
			continue ;
  }
  m_contacts_count++;
	return ;
}

void PhoneBook::SearchContact()
{
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  std::cin.clear();

  if (m_contacts_count == 0)
  {
    std::cout << "No notes" << std::endl;
    return;
  }

  for (int i = 0; i < m_contacts_count; ++i)
    _ShowContactShort(i);
  
  std::cout << "Ented desired id:";
  int desired_note;
  if (!_GetNumberFromInput(desired_note))
  {
    std::cout << "Bad input" << std::endl;
    return ;
  }

  if (desired_note > m_contacts_count || desired_note <= 0)
  {
    std::cout << "Bad id" << std::endl;
    return;
  }

  _ShowContactFull(desired_note - 1);
}

void PhoneBook::_ShowContactFull(int i_index)
{
  for (int i = 0; i < 11; ++i)
	{
    _PrintField(m_contacts[i_index].field[i]);
    if (i < 10)
			std::cout << "|";
	}
	std::cout << std::endl;
}

void PhoneBook::_ShowContactShort(int i_index)
{
  if (i_index > m_contacts_count)
    return ; 
	std::cout << std::setw(10);
  std::cout << i_index + 1;
  std::cout << "|";
  _PrintField(m_contacts[i_index].field[0]);
  std::cout << "|";
  _PrintField(m_contacts[i_index].field[1]);
  std::cout << "|";
  _PrintField(m_contacts[i_index].field[2]);
  std::cout << std::endl;
}

