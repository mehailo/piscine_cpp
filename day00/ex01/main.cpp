#include "PhoneBook.hpp"

bool get_input(std::string &i_str)
{
  std::cout << "Enter the command: ";
  i_str.erase();
  std::cin.clear();
  if (std::cin >> i_str)
    return true;
  else
    return false;
}

int main()
{
    PhoneBook phonebook;
    std::string command;

    while (get_input(command))
    {
        if (command.size() == 0)
          continue ;
        else if (!command.compare("ADD"))
          phonebook.AddContact();
        else if (!command.compare("SEARCH"))
          phonebook.SearchContact();
        else if (!command.compare("EXIT"))
            break;
    }
}