#include <iostream>
#include <string>
#include <locale>

int main(int argc, char **argv)
{
	std::string word;

	if (argc == 1)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
		return 1;
	}
	for (int j = 1; j < argc; j++)
	{
		word = argv[j];
		for (size_t i = 0; i < word.size(); i++)
		{
			std::cout << (char) std::toupper(word[i]);
		}
	}
	std::cout << std::endl;
	return 0;
}