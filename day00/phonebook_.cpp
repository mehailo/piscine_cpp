#include "phonebook_.hpp"

void		print_field(std::string str)
{
	if (str.length() > 10)
	{
		std::cout << std::setw(10);
		std::cout << str.substr(0, 9) << ".";
	}
	else
	{
		std::cout << std::setw(11);
		std::cout << str;
	}
	return ;
}

void		adding_note(struct s_phonebook phonebook[8], int count_notes)
{
	char tmp[256];
	std::string fields_titles[] = {
		"first name", 
		"last name", 
		"nickname", 
		"login", 
		"postal address", 
		"email address", 
		"phone number", 
		"birthday date", 
		"favorite meal", 
		"underwear color" , 
		"darkest secret"};

	for (int j = 0; j < 11; ++j)
	{
		std::cout << fields_titles[j] << ": ";
		std::cin.getline(tmp, sizeof(tmp), '\n');
		phonebook[count_notes].field[j] += tmp;
		if (phonebook[count_notes].field[j].length() == 0)
			continue ;
		bzero(&tmp, 256);
	}
	return ;
}

void		show_note(struct s_phonebook phonebook[8], int note)
{
	int j;

	j = 0;
	while (j < 11)
	{
		print_field(phonebook[note - 1].field[j]);
		if (j < 10)
			std::cout << "|";
		j++;
	}
	std::cout << std::endl;
}

void		show_all_notes(struct s_phonebook phonebook[8], int num_of_notes)
{
	int i;

	i = 0;
	while (i < num_of_notes)
	{
		std::cout << std::setw(10);
		std::cout << i + 1;
		std::cout << "|";
		print_field(phonebook[i].field[0]);
		std::cout << "|";
		print_field(phonebook[i].field[1]);
		std::cout << "|";
		print_field(phonebook[i].field[2]);
		std::cout << std::endl;
		i++;
	}
}

int main()
{
	struct s_phonebook	phonebook[8];
	std::string			command;
	int					count_notes;
	int					number_of_note;

	count_notes = 0;
	bzero(&phonebook, sizeof(struct s_phonebook));
	while (42)
	{
		std::cout << "Enter the command: ";
		std::cin >> command;
		if (!command.compare("ADD"))
		{
			if (count_notes == 8)
				std::cout << "640K isn't enough for everybody. There are too many records\n";
			else
			{
				adding_note(phonebook, count_notes);
				count_notes++;
			}
		}
		else if (!command.compare("SEARCH"))
		{
			show_all_notes(phonebook, count_notes);
			std::cout << "Enter the index of desired person:";
			std::cin >> number_of_note;
			if (number_of_note > count_notes)
				std::cout << "There is no note with this index" << std::endl;
			else
				show_note(phonebook, number_of_note);
		}
		else if (!command.compare("EXIT"))
			break ;
		else if (command.length() == 0)
			continue ;
	}
	bzero(&phonebook, sizeof(struct s_phonebook));
}
