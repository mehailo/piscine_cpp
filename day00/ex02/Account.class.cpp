//
// Created by Mykhailo Frankevich on 5/4/17.
//

#include "Account.class.hpp"
#include <ctime>
#include <iostream>

int	Account::_nbAccounts = 0;
int	Account::_totalAmount = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

Account::Account(int initial_deposit) : _accountIndex(_nbAccounts), _amount(initial_deposit)
{
	this->_nbDeposits = 0;
	this->_nbWithdrawals = 0;

	this->_nbAccounts += 1;
	this->_totalAmount += initial_deposit;

	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";created" << std::endl;
	return ;
}

Account::~Account(void)
{
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";closed" << std::endl;
}

void	Account::_displayTimestamp( void ) {
	std::time_t t = std::time(NULL);
	char str[18];
	if (std::strftime(str, sizeof(str), "[%Y%d%m_%H%M%S]", std::localtime(&t))) {
		std::cout << str;
	}
}

int		Account::getNbAccounts( void ) {
	return(Account::_nbAccounts);
}

int		Account::getTotalAmount( void ) {
	return(Account::_totalAmount);
}

int		Account::getNbDeposits( void ) {
	return(Account::_totalNbDeposits);
}

int		Account::getNbWithdrawals( void ) {
	return(Account::_totalNbWithdrawals);
}

void	Account::displayAccountsInfos( void ) {
	Account::_displayTimestamp();
	std::cout << " accounts:" << Account::_nbAccounts
			  << ";total:" << Account::_totalAmount
			  << ";deposits:" << Account::_totalNbDeposits
			  << ";withdrawals:" << Account::_totalNbWithdrawals
			  << std::endl;
}

void	Account::makeDeposit( int deposit ) {
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex
			  << ";p_amount:" << this->_amount
			  << ";deposit:" << deposit
			  << ";amount:" << this->_amount + deposit
			  << ";nb_deposits:" << this->_nbDeposits + 1
			  << std::endl;
	this->_nbDeposits++;
	this->_totalNbDeposits++;
	this->_totalAmount += deposit;
	this->_amount += deposit;
}

bool	Account::makeWithdrawal( int withdrawal ) {
	Account::_displayTimestamp();
	if (withdrawal > this->_amount)
	{
		std::cout << " index:" << this->_accountIndex
				  << ";p_amount:" << this->_amount
				  << ";withdrawal:refused"
				  << std::endl;
		return false;
	}
	std::cout << " index:" << this->_accountIndex
			  << ";p_amount:" << this->_amount
			  << ";withdrawal:" << withdrawal
			  << ";amount:" << this->_amount - withdrawal
			  << ";nb_withdrawals:" << this->_nbWithdrawals + 1
			  << std::endl;
	this->_nbWithdrawals++;
	this->_totalNbWithdrawals++;
	this->_totalAmount -= withdrawal;
	this->_amount -= withdrawal;
	return true;
}

int		Account::checkAmount( void ) const {
	return (this->_amount);
}

void	Account::displayStatus( void ) const {
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex
			  << ";amount:" << this->_amount
			  << ";deposits:" << this->_nbDeposits
			  << ";withdrawals:" << this->_nbWithdrawals
			  << std::endl;
}
