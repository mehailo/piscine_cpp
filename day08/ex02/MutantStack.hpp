#include <stdio>
#include <string>

template <typename T>
struct MutantStack {
	MutantStack();
	~MutantStack();

	MutantStack & operator=(MutantStack const & stack);
	MutantStack(MutantStack const & stack);
};