//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_AWEAPON_HPP
#define PISCINE_CPP_AWEAPON_HPP

#include <string>
#include <iostream>
#include <sstream>

class AWeapon
{
private:
	std::string _name;
	int _apcost;
	int _damage;

public:
	AWeapon();
	AWeapon(std::string const & name, int apcost, int damage);

	AWeapon(const AWeapon & aweapon);
	void operator=(const AWeapon & rhs);

	virtual ~AWeapon();

	std::string const & getName() const;
	int getAPcost() const;
	int getDamage() const;

	virtual void attack() const = 0;
};


#endif //PISCINE_CPP_AWEAPON_HPP
