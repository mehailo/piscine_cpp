//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_POWERFIST_HPP
#define PISCINE_CPP_POWERFIST_HPP


#include "AWeapon.hpp"

class PowerFist : public AWeapon
{
public:
	PowerFist();

	virtual ~PowerFist();

	void attack() const;
};

#endif //PISCINE_CPP_POWERFIST_HPP


