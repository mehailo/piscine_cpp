//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Character.hpp"

int Character::get_ap() const
{
	return _ap;
}

AWeapon *Character::get_weapon() const { return _weapon; }

Character::Character()
{
	this->_name = "unnamed";
	this->_ap = 40;
	this->_weapon = NULL;

	return ;
}

Character::Character(std::string const &name)
{
	this->_name = name;
	this->_ap = 40;
	this->_weapon = NULL;

	return ;
}

Character::~Character()
{
	return ;
}

void Character::recoverAP()
{
	this->_ap += 10;
	this->_ap = (this->_ap > 40 ? 40 : this->_ap);

	return ;
}

void Character::equip(AWeapon *weapon)
{
	this->_weapon = weapon;

	return ;
}

std::string const &Character::getName() const
{
	return this->_name;
}

void Character::attack(Enemy *target)
{
	if (this->_ap > _weapon->getAPcost())
	{
		std::cout << this->_name << " attacks " << target->getType() << " with a " << this->_weapon->getName() << std::endl;
		_weapon->attack();
		this->_ap -= _weapon->getAPcost();
		target->takeDamage(_weapon->getDamage());
		if (target->getHP() <= 0)
			delete target;
	}


	return ;
}

std::ostream &operator<<(std::ostream &os, const Character &character)
{
	if (character.get_weapon() == NULL)
		os << character.getName() << " has " << character.get_ap() << " AP "
		   << "and is unarmed" << std::endl;
	else
		os << character.getName() << " has " << character.get_ap() << " AP"
	 	  << " and wields a " << character.get_weapon()->getName() << std::endl;
	return os;
}
