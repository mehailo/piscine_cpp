//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "AWeapon.hpp"

AWeapon::AWeapon()
{
	this->_name = "";
	this->_apcost = 0;
	this->_damage = 0;

	return ;
}

AWeapon::AWeapon(std::string const &name, int apcost, int damage)
{
	this->_name = name;
	this->_apcost = apcost;
	this->_damage = damage;

	return ;
}

AWeapon::AWeapon(const AWeapon &aweapon)
{
	*this = aweapon;

	return ;
}

void AWeapon::operator=(const AWeapon &rhs)
{
	this->_name = rhs.getName();
	this->_apcost = rhs.getAPcost();
	this->_damage = rhs.getDamage();

	return ;
}

AWeapon::~AWeapon()
{
	return ;
}

const std::string &AWeapon::getName() const { return _name; }

int AWeapon::getAPcost() const { return _apcost; }

int AWeapon::getDamage() const { return _damage; }
