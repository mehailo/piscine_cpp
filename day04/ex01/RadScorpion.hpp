//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_RADSCORPION_HPP
#define PISCINE_CPP_RADSCORPION_HPP


#include "Enemy.hpp"

class RadScorpion : public Enemy
{
public:
	RadScorpion();

public:
	virtual ~RadScorpion();

};

#endif //PISCINE_CPP_RADSCORPION_HPP

