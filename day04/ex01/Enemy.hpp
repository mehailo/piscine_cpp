//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_ENEMY_HPP
#define PISCINE_CPP_ENEMY_HPP

#include <string>
#include <iostream>
#include <sstream>

class Enemy
{
private:
	int _hp;
	std::string _type;

public:
	Enemy(int hp, std::string const & type);

	Enemy(const Enemy & enemy);
	void operator=(const Enemy & enemy);

	Enemy();

	virtual ~Enemy();



	std::string const & getType() const;
	int getHP() const;

	virtual void takeDamage(int);
};


#endif //PISCINE_CPP_ENEMY_HPP
