//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_PLASMARIFLE_HPP
#define PISCINE_CPP_PLASMARIFLE_HPP

#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon
{
public:
	PlasmaRifle();

	virtual ~PlasmaRifle();

	void attack() const;
};


#endif //PISCINE_CPP_PLASMARIFLE_HPP
