//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Enemy.hpp"

Enemy::Enemy()
{
	return ;
}

Enemy::Enemy(int hp, std::string const &type)
{
	this->_hp = hp;
	this->_type = type;

	return ;
}

Enemy::~Enemy()
{
	return ;
}

std::string const & Enemy::getType() const { return this->_type; }

int Enemy::getHP() const { return this->_hp; }

void Enemy::takeDamage(int damage) {
	if (this->_hp >= 0)
		this->_hp -= damage;

	return ;
}

Enemy::Enemy(const Enemy &enemy) {
	*this = enemy;

	return ;
}

void Enemy::operator=(const Enemy &enemy) {
	this->_hp = enemy.getHP();
	this->_type = enemy.getType();

	return ;
}
