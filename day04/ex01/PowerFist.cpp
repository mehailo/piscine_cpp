//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "PowerFist.hpp"

void PowerFist::attack() const
{
	std::cout << "* pschhh... SBAM! *" <<std::endl;
}

PowerFist::~PowerFist()
{
	return ;
}

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50)
{
	return ;
}