//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_CHARACTER_HPP
#define PISCINE_CPP_CHARACTER_HPP

#include <string>
#include <iostream>
#include <sstream>
#include "AWeapon.hpp"
#include "PowerFist.hpp"
#include "PlasmaRifle.hpp"
#include "Enemy.hpp"
#include "RadScorpion.hpp"
#include "SuperMutant.hpp"

class Character
{
private:
	std::string _name;
	int _ap;
	AWeapon *_weapon;
public:
	Character(std::string const & name);

	~Character();

	void recoverAP();

	Character();

	void equip(AWeapon*);

	int get_ap() const;
	std::string const & getName() const;

	void attack(Enemy*);

	AWeapon *get_weapon() const;
};

std::ostream &
operator<<(std::ostream &os, const Character &character);

#endif //PISCINE_CPP_CHARACTER_HPP
