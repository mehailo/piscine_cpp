//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_SUPERMUTANT_HPP
#define PISCINE_CPP_SUPERMUTANT_HPP

#include "Enemy.hpp"

class SuperMutant : public Enemy
{
public:
	SuperMutant();

	virtual ~SuperMutant();

	void takeDamage(int i);

};


#endif //PISCINE_CPP_SUPERMUTANT_HPP