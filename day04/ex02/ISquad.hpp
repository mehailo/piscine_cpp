//
// Created by Mykhailo Frankevich on 5/11/17.
//

#ifndef PISCINE_CPP_ISQUAD_HPP
#define PISCINE_CPP_ISQUAD_HPP

#include "ISpaceMarine.hpp"
#include "string"
#include "iostream"

class ISquad
{
public:
	virtual ~ISquad() {}
	virtual int getCount() const = 0;
	virtual ISpaceMarine* getUnit(int) const = 0;
	virtual int push(ISpaceMarine*) = 0;
};

#endif //PISCINE_CPP_ISQUAD_HPP
