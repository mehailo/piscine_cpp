//
// Created by Mykhailo Frankevich on 5/11/17.
//

#ifndef PISCINE_CPP_SQUAD_HPP
#define PISCINE_CPP_SQUAD_HPP

#include "ISquad.hpp"

class Squad : public ISquad
{
public:

	Squad();

	~Squad();

	Squad(const Squad & squad);

	void operator=(const Squad & rhs);

	ISpaceMarine *get_marines() const;

	int getCount() const;

	ISpaceMarine *getUnit(int i) const;

	int push(ISpaceMarine *marine);


private:

	ISpaceMarine **_marines;

	int _count;

	void delete_marines();
};


#endif //PISCINE_CPP_SQUAD_HPP
