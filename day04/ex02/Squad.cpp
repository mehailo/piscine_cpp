//
// Created by Mykhailo Frankevich on 5/11/17.
//

#include "Squad.hpp"

Squad::Squad()
{
	this->_count = 0;

	return ;
}

Squad::~Squad()
{
	delete_marines();

	return ;
}

Squad::Squad(const Squad &squad)
{
	*this = squad;

	return;
}

void Squad::operator=(const Squad &rhs)
{
	*this->_marines = rhs.get_marines();

	return ;
}

ISpaceMarine *Squad::get_marines() const
{
	return *_marines;
}

int Squad::getCount() const
{
	return this->_count;
}

ISpaceMarine *Squad::getUnit(int i) const
{
	return this->_marines[i];
}

int Squad::push(ISpaceMarine *marine)
{
	ISpaceMarine **p = new ISpaceMarine*[this->_count + 1];
	for (int i = 0; i < this->_count; i++)
	{
		p[i] = this->_marines[i]->clone();
	}
	p[this->_count] = marine->clone();
//	delete_marines();
	this->_marines = p;
	this->_count++;

	return (this->_count);
}

void Squad::delete_marines(void)
{
	for (int i = 0; i < this->_count; i++)
	{
		delete this->_marines[i];
	}

	return ;
}
