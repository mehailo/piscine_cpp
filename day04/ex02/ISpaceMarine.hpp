//
// Created by Mykhailo Frankevich on 5/11/17.
//

#ifndef PISCINE_CPP_ISPACEMARINE_HPP
#define PISCINE_CPP_ISPACEMARINE_HPP

#include "string"
#include "iostream"

class ISpaceMarine
{
public:
	virtual ~ISpaceMarine() {}
	virtual ISpaceMarine* clone() const = 0;
	virtual void battleCry() const = 0;
	virtual void rangedAttack() const = 0;
	virtual void meleeAttack() const = 0;
};


#endif //PISCINE_CPP_ISPACEMARINE_HPP
