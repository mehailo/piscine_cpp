//
// Created by Mykhailo Frankevich on 5/11/17.
//

#ifndef PISCINE_CPP_TACTICALMARINE_HPP
#define PISCINE_CPP_TACTICALMARINE_HPP

#include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine
{
public:
	TacticalMarine();

	~TacticalMarine();

	TacticalMarine(const TacticalMarine & TacticalMarine);

	void operator=(const TacticalMarine & rhs);

	void battleCry() const;

	void rangedAttack() const;

	void meleeAttack() const;

	ISpaceMarine *clone() const;
};


#endif //PISCINE_CPP_TACTICALMARINE_HPP
