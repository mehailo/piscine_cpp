//
// Created by Mykhailo Frankevich on 5/11/17.
//

#include "AssaultTerminator.hpp"

ISpaceMarine *AssaultTerminator::clone() const
{
	ISpaceMarine *a = new AssaultTerminator(*this);
	return a;
}

void AssaultTerminator::battleCry() const
{
	std::cout << "This code is unclean. PURIFY IT !" << std::endl;
}

void AssaultTerminator::rangedAttack() const
{
	std::cout << "* does nothing *" << std::endl;
}

void AssaultTerminator::meleeAttack() const
{
 std::cout << "* attacks with chainfists *" << std::endl;
}

AssaultTerminator::AssaultTerminator()
{
	std::cout << "* teleports from space *" << std::endl;
}

AssaultTerminator::~AssaultTerminator()
{
	std::cout << "I'll be back ..." << std::endl;
}

AssaultTerminator::AssaultTerminator(const AssaultTerminator &assaultTerminator)
{
	return ;
}

void AssaultTerminator::operator=(const AssaultTerminator &rhs)
{
	return ;
}
