//
// Created by Mykhailo Frankevich on 5/11/17.
//

#ifndef PISCINE_CPP_ASSAULTTERMINATOR_HPP
#define PISCINE_CPP_ASSAULTTERMINATOR_HPP


#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine
{
public:
	AssaultTerminator();

	~AssaultTerminator();

	AssaultTerminator(const AssaultTerminator & assaultTerminator);

	void operator=(const AssaultTerminator & rhs);

	void battleCry() const;

	void rangedAttack() const;

	void meleeAttack() const;

	ISpaceMarine *clone() const;
};


#endif //PISCINE_CPP_ASSAULTTERMINATOR_HPP
