//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_PEON_HPP
#define PISCINE_CPP_PEON_HPP


#include "Victim.hpp"

class Peon : public Victim
{
public:
	Peon();

	~Peon();

	Peon(const std::string &name);

	void getPolymorphed() const;
};

std::ostream &operator<<(std::ostream &os, const Peon &peon);


#endif //PISCINE_CPP_PEON_HPP
