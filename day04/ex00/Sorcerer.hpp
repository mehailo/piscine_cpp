//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_SORCERER_HPP
#define PISCINE_CPP_SORCERER_HPP

#include <string>
#include <iostream>
#include <sstream>
#include "Victim.hpp"

class Sorcerer
{
public:
	Sorcerer();
	Sorcerer( std::string name, std::string title );

	~Sorcerer();

	const std::string &getName() const;
	const std::string &getTitle() const;

	Sorcerer(const Sorcerer & old);
	void operator=(const Sorcerer &rhs);

	void polymorph(Victim const &) const;

protected:
	std::string name;
	std::string title;
};

std::ostream &operator<<(std::ostream &os, const Sorcerer &sorcerer);


#endif //PISCINE_CPP_SORCERER_HPP
