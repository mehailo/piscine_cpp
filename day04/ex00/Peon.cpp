//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Peon.hpp"

Peon::Peon() : Victim()
{
	return;
}

Peon::Peon(const std::string &name) : Victim(name)
{
	std::cout << "Zog zog." << std::endl;

	return ;
}

Peon::~Peon()
{
	std::cout << "Bleuark..." << std::endl;
}

void Peon::getPolymorphed() const
{
	std::cout << name << " has been turned into a pink pony !" << std::endl;

	return ;
}

std::ostream &operator<<(std::ostream &os, const Peon &peon)
{
	os << "I'm " << peon.getName() << " and I like otters !" << std::endl;
	return os;
}