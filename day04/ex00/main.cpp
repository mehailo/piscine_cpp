//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Sorcerer.hpp"
#include "Victim.hpp"
#include "Peon.hpp"

int main()
{

	Sorcerer robert("Robert", "the Magnificent");
	Victim jim("Jimmy");
	Peon joe("Joe");
	std::cout << robert << jim << joe;
	robert.polymorph(jim);
	robert.polymorph(joe);
	std::cout << robert.getName() << " " << robert.getTitle() << std::endl;
	Peon a("Rainbow Dash");
	Victim d("Red");
	Victim *c = new Peon("ASasdasdasdasd");
	std::cout << a;
	std::cout << *c;
	std::cout << d;
	robert.polymorph(*c);
	robert.polymorph(a);
	robert.polymorph(d);
	return 0;
}
