//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Victim.hpp"

Victim::~Victim()
{
	std::cout << "Victim " << name
			  << " just died for no apparent reason !" << std::endl;
}

const std::string &Victim::getName() const { return name; }

Victim::Victim()
{
	Victim obj("unnamed");
	*this = obj;

	return ;
}

Victim::Victim(std::string name)
{
	std::cout << "Some random victim called " << name
			  << " just popped !" << std::endl;

	this->name = name;
}

Victim::Victim(const Victim &old)
{
	Victim obj("unnamed");

	*this = obj;
}

void Victim::operator=(const Victim &old)
{
	this->name = old.getName();
}

void Victim::getPolymorphed() const
{
	std::cout << name << " has been turned into a cute little sheep !" << std::endl;

	return ;
}

std::ostream &operator<<(std::ostream &os, const Victim &victim)
{
	os << "I'm " << victim.getName() << " and I like otters !" << std::endl;
	return os;
}
