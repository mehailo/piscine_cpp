//
// Created by Mykhailo Frankevich on 5/10/17.
//

#include "Sorcerer.hpp"

Sorcerer::Sorcerer()
{
	Sorcerer old("unnamed", "untitled");

	*this = old;

	return;
}

Sorcerer::~Sorcerer()
{
	std::cout << name << ", " << title
			  << ", is dead. Consequences will never be the same !" << std::endl;

	return ;
}

Sorcerer::Sorcerer(std::string name, std::string title)
{
	std::cout << name << ", "
			  << title << ", is born !" << std::endl;

	this->name = name;
	this->title = title;

	return ;
}

Sorcerer::Sorcerer(const Sorcerer &old)
{
	*this = old;

	return ;
}

void Sorcerer::operator=(const Sorcerer &rhs)
{
	this->name = getName();
	this->title = getTitle();

	return ;
}

const std::string &Sorcerer::getName() const { return name; }

const std::string &Sorcerer::getTitle() const { return title; }

void Sorcerer::polymorph(const Victim &obj) const
{
	obj.getPolymorphed();
}

std::ostream &operator<<(std::ostream &os, const Sorcerer &sorcerer)
{
	os << "I am " << sorcerer.getName() << ", " << sorcerer.getTitle() << ", and I like ponies !" << std::endl;
	return os;
}
