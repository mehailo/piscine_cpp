//
// Created by Mykhailo Frankevich on 5/10/17.
//

#ifndef PISCINE_CPP_VICTIM_HPP
#define PISCINE_CPP_VICTIM_HPP

#include <string>
#include <iostream>
#include <sstream>

class Victim
{
public:
	Victim();
	Victim(std::string name);

	virtual ~Victim();

	Victim(const Victim & old);
	void operator=(const Victim & old);

	const std::string &getName() const;

	virtual void getPolymorphed() const;

protected:
	std::string name;

};

std::ostream &operator<<(std::ostream &os, const Victim &victim);


#endif //PISCINE_CPP_VICTIM_HPP
