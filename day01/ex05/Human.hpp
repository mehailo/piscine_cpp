//
// Created by Mykhailo Frankevich on 5/5/17.
//

#ifndef PISCINE_CPP_HUMAN_HPP
#define PISCINE_CPP_HUMAN_HPP

#include "Brain.hpp"

class Human
{
private:
	Brain const _brain;
public:
	Human();
	~Human();
	std::string identify(void) const;
	const Brain& getBrain();
};

#endif //PISCINE_CPP_HUMAN_HPP
