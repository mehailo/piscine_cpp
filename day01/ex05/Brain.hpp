//
// Created by Mykhailo Frankevich on 5/5/17.
//

#ifndef PISCINE_CPP_BRAIN_HPP
#define PISCINE_CPP_BRAIN_HPP

#include <string>
#include <iostream>
#include <sstream>

class Brain
{
public:
	Brain();
	~Brain();
	int iq;
	std::string const identify(void) const;
};

#endif //PISCINE_CPP_BRAIN_HPP