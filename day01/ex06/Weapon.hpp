//
// Created by Mykhailo Frankevich on 5/5/17.
//

#ifndef PISCINE_CPP_WEAPON_HPP
#define PISCINE_CPP_WEAPON_HPP

#include <string>
#include <iostream>

class Weapon
{
public:
	Weapon(std::string type);

	virtual ~Weapon();

private:

	std::string type;

public:

	const std::string &getType() const;

	void setType(const std::string &type);
};

#endif //PISCINE_CPP_WEAPON_HPP
