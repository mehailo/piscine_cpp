//
// Created by Mykhailo Frankevich on 5/5/17.
//

#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon& weapon) : name(name), _weapon(weapon)
{
	return ;
}

HumanA::~HumanA()
{
	return ;
}

const Weapon &HumanA::get_weapon() const
{
	return _weapon;
}

const std::string &HumanA::getName() const
{
	return name;
}

void HumanA::set_weapon(Weapon _weapon)
{
	HumanA::_weapon = _weapon;
}

void HumanA::attack()
{
	std::cout << this->name << " attacs with his " << this->get_weapon().getType() << std::endl;
};