//
// Created by Mykhailo Frankevich on 5/5/17.
//

#include "HumanB.hpp"

HumanB::HumanB(std::string name) : name(name)
{
	this->_weapon = NULL;
	return ;
}

HumanB::~HumanB()
{
	return ;
}

const Weapon &HumanB::get_weapon() const
{
	return *_weapon;
}

const std::string &HumanB::getName() const
{
	return name;
}

void HumanB::setWeapon(Weapon &_weapon)
{
	HumanB::_weapon = &_weapon;
}

void HumanB::attack()
{
	std::cout << this->name << " attacs with his " << this->get_weapon().getType() << std::endl;
}