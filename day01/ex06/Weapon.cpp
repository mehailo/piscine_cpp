//
// Created by Mykhailo Frankevich on 5/5/17.
//

#include "Weapon.hpp"


Weapon::Weapon(std::string type)
{
	this->type = type;
	return ;
}

Weapon::~Weapon()
{
	return ;
}

const std::string &Weapon::getType() const
{
	return type;
}

void Weapon::setType(const std::string &type)
{
	Weapon::type = type;
}
