//
// Created by Mykhailo Frankevich on 5/5/17.
//

#ifndef PISCINE_CPP_HUMANB_HPP
#define PISCINE_CPP_HUMANB_HPP

#include "HumanA.hpp"

class HumanB
{
public:
	HumanB(std::string name);

	const Weapon &get_weapon() const;

	const std::string &getName() const;

	void setWeapon(Weapon &_weapon);

	virtual ~HumanB();

	void attack();

private:
	Weapon *_weapon;
	std::string name;
};


#endif //PISCINE_CPP_HUMANB_HPP
