//
// Created by Mykhailo Frankevich on 5/5/17.
//

#ifndef PISCINE_CPP_HUMANA_HPP
#define PISCINE_CPP_HUMANA_HPP

#include "Weapon.hpp"

class HumanA
{
public:
	HumanA(std::string name, Weapon& weapon);

	const Weapon &get_weapon() const;

	const std::string &getName() const;

	void set_weapon(Weapon _weapon);

	virtual ~HumanA();

	void attack();

private:
	Weapon &_weapon;
	std::string name;
};


#endif //PISCINE_CPP_HUMANA_HPP
