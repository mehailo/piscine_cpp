//
// Created by Mykhailo Frankevich on 5/6/17.
//

#ifndef PISCINE_CPP_HUMAN_HPP
#define PISCINE_CPP_HUMAN_HPP

#include <string>
#include <iostream>

class Human
{
private:
	void meleeAttack(std::string const & target);
	void rangedAttack(std::string const & target);
	void intimidatingShout(std::string const & target);
public:
	void action(std::string const & action_name, std::string const & target);
};


#endif //PISCINE_CPP_HUMAN_HPP
