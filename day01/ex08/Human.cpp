//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "Human.hpp"

void Human::meleeAttack(std::string const & target)
{
	std::cout << "Performing the melee attack " << target << std::endl;
}

void Human::rangedAttack(std::string const & target)
{
	std::cout << "Performing the ranged attack on " << target << std::endl;
}

void Human::intimidatingShout(std::string const & target)
{
	std::cout << "FUS RO DAH!!! " << target << std::endl;
}

void Human::action(std::string const & action_name, std::string const & target)
{
	std::string actions[3] = {"melee", "ranged", "shout"};
	void (Human::*perform_actions[])(std::string const&) = {&Human::meleeAttack, &Human::rangedAttack, &Human::intimidatingShout};
	for (int i = 0; i < 3; i++)
	{
		if (!actions[i].compare(action_name))
		{
			(this->*perform_actions[i])(target);
		}
	}
}
