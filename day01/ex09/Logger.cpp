//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "Logger.hpp"

Logger::Logger(){
	this->_filename = this->_timestamp();
}

std::string		Logger::_timestamp( void ) {
	std::time_t t = std::time(NULL);
	char str[20];

	std::strftime(str, sizeof(str), "%Y_%d_%m %H:%M:%S", std::localtime(&t));
	std::string result(str);
	return (result);
}

void Logger::log(std::string const & dest, std::string const & message){
	std::string actions[2] = {"console", "file"};
	void (Logger::*perform_actions[])(std::string const&) = {&Logger::logToConsole, &Logger::logToFile};
	for (int i = 0; i < 2; i++)
	{
		if (!actions[i].compare(dest))
		{
			(this->*perform_actions[i])(message);
		}
	}
}

std::string Logger::_create_log_entry(std::string const & message)
{
	std::string result;

	result += this->_timestamp();
	result += " ";
	result += message;
	return (result);
}

void Logger::logToConsole( std::string const & message)
{
	std::string result = this->_create_log_entry(message);

	std::cout << result << std::endl;
}

void Logger::logToFile( std::string const & message)
{
	std::ofstream file(this->_filename, std::ios::out | std::ios::app);
	std::string result = this->_create_log_entry(message);

	file << result;
	file << std::endl;

	file.close();
}