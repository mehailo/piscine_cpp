//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "Logger.hpp"

int main()
{
	Logger logger;

	logger.log("file", "howdy");
	logger.log("file", "yaaay");
	logger.log("file", "smth bad");
	logger.log("console", "howdy");
	logger.log("console", "yaaay");
	logger.log("console", "smth bad");
	return (0);
}
