//
// Created by Mykhailo Frankevich on 5/6/17.
//

#ifndef PISCINE_CPP_LOGGER_HPP
#define PISCINE_CPP_LOGGER_HPP

# include <string>
# include <iostream>
# include <fstream>
# include <streambuf>

class Logger
{
private:
	std::string _filename;
	void logToConsole( std::string const & );
	void logToFile( std::string const & );
	std::string _timestamp( void );
	std::string _create_log_entry( std::string const & );
public:
	Logger();
	void log( std::string const &, std::string const & );
};


#endif //PISCINE_CPP_LOGGER_HPP
