//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include <string>
#include <iostream>

void memoryLeak()
{
	std::string* panthere = new std::string("String panthere");
	std::cout << *panthere << std::endl;
	delete panthere;
}
//
//int main(void)
//{
//	memoryLeak();
//	for(;;){}
//}
