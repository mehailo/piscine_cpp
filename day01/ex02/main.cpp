//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include "ZombieEvent.hpp"
#include <time.h>
#include <stdlib.h>
#include <random>
#include <cstdlib>
#include <iostream>
#include <ctime>

std::string generate_random_string()
{
	std::string name = "arry";
	char first_letter = 'A';
	static int i = 0;
	int		r;

	srand(time(NULL));
	r = ((rand() + ((int)(26 - 0 + 1) + i++)) % 26);
	first_letter += r;
	std::string appendix(&first_letter);
	name.insert(0, appendix);
	return (name);
}

Zombie *randomChump()
{
	Zombie *zombie = new Zombie(generate_random_string());
	zombie->announce();
	return zombie;
}

void	set_zombie_type(std::string type)
{
	Zombie::type_of_zombie = type;
}

int main()
{
	Zombie *zombie;
	ZombieEvent event;

	event.setZombieType("green crawler");
	zombie = randomChump();
}
