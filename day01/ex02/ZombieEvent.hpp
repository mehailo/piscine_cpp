//
// Created by Mykhailo Frankevich on 5/3/17.
//

#ifndef PISCINE_CPP_ZOMBIEEVENT_HPP
#define PISCINE_CPP_ZOMBIEEVENT_HPP

#include "Zombie.hpp"

class ZombieEvent
{
public:
	void setZombieType(std::string type);
	Zombie* newZombie(std::string name);
};


#endif //PISCINE_CPP_ZOMBIEEVENT_HPP
