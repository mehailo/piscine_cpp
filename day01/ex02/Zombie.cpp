//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include "Zombie.hpp"

std::string		Zombie::type_of_zombie = "green";

void Zombie::announce(void)
{
	std::cout << this->name << "(" << this->type << ")"
			  << " Braiiiiiiinnnssss..." << std::endl;
}

Zombie::Zombie(std::string name)
{
	this->name = name;
	type = Zombie::type_of_zombie;
}