//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include "ZombieEvent.hpp"

void	ZombieEvent::setZombieType(std::string type)
{
	Zombie::type_of_zombie = type;
}

Zombie* ZombieEvent::newZombie(std::string name)
{
	Zombie *zombie = new Zombie(name);
	return zombie;
}