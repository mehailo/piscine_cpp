//
// Created by Mykhailo Frankevich on 5/5/17.
//

#include "iostream"
#include <string>

int main()
{
	std::string str = "HI THIS IS BRAIN";
	std::string* pointer = &str;
	std::string& reference = str;

	std::cout << *pointer << std::endl;
	std::cout << reference << std::endl;
}
