//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "ex07.hpp"

std::string string_to_upper(std::string strToConvert)
{
	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

	return strToConvert;
}

bool is_file_exist(const char *fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

int main(int argc, char **argv)
{
	if (argc != 4 || !is_file_exist(argv[1]))
	{
		std::cerr << "There must be turned in :\n "
				"1) File \n "
				"2) String that will be replaced \n "
				"3) New shinny and much better string that will replace that crappy one. \n"
				"Strings can't be empty. Have Fun." << std::endl;
		return (1);
	}

	std::string name(argv[1]);
	name = string_to_upper(name);
	name += ".replace" ;
	std::ofstream file(name);

	std::ifstream input(argv[1]);
	std::string str((std::istreambuf_iterator<char>(input)),
					std::istreambuf_iterator<char>());

	while (str.find(argv[2]) != std::string::npos)
		str.replace(str.find(argv[2]), sizeof(argv[3]) - 1, argv[3]);

	file << str;
	return(0);
}

