//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include "Zombie.hpp"

std::string		Zombie::type_of_zombie = "green";

void Zombie::announce(void)
{
	std::cout << this->name << "(" << this->type << ")"
			  << " Braiiiiiiinnnssss..." << std::endl;
}

Zombie::Zombie(std::string name)
{
	this->name = name;
	type = Zombie::type_of_zombie;
//	this->announce();
}

Zombie::Zombie()
{
	this->name = Zombie::_generate_random_name();
	type = Zombie::type_of_zombie;
//	this->announce();
}

std::string Zombie::_generate_random_name()
{
	std::string name = "arry";
	char first_letter = 'A';
	static int i = 0;
	int		r;

	srand(time(NULL));
	r = ((rand() + ((int)(26 - 0 + 1) + i++)) % 26);
	first_letter += r;
	std::string appendix(&first_letter);
	name.insert(0, appendix);
	return (name);
}