//
// Created by Mykhailo Frankevich on 5/3/17.
//

#ifndef PISCINE_CPP_ZOMBIE_HPP
#define PISCINE_CPP_ZOMBIE_HPP

#include <string>
#include <iostream>

class Zombie
{
public:
	static std::string type_of_zombie;
	std::string name;
	std::string type;
	Zombie(std::string name);
	Zombie();
	void announce();

private:
	std::string _generate_random_name();
};


#endif //PISCINE_CPP_ZOMBIE_HPP
