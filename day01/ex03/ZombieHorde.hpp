//
// Created by Mykhailo Frankevich on 5/4/17.
//

#ifndef PISCINE_CPP_ZOMBIEHORDE_HPP
#define PISCINE_CPP_ZOMBIEHORDE_HPP

#include "Zombie.hpp"
#include <vector>

class ZombieHorde
{
public:
	ZombieHorde(int n);
	~ZombieHorde(void);

private:
	std::vector<Zombie> _zombies;
	Zombie *_zombies_army;
	std::string _generate_random_name();
};

#endif //PISCINE_CPP_ZOMBIEHORDE_HPP
