//
// Created by Mykhailo Frankevich on 5/4/17.
//

#include "ZombieHorde.hpp"

std::string ZombieHorde::_generate_random_name()
{
	std::string name = "arry";
	char first_letter = 'A';
	static int i = 0;
	int		r;

	srand(time(NULL));
	r = ((rand() + ((int)(26 - 0 + 1) + i++)) % 26);
	first_letter += r;
	std::string appendix(&first_letter);
	name.insert(0, appendix);
	return (name);
}

ZombieHorde::ZombieHorde(int n)
{
	this->_zombies_army = new Zombie[n];
	for (int i = 0; i < n ; i++)
	{
		this->_zombies_army[i].announce();
	}
}

ZombieHorde::~ZombieHorde(void)
{
	delete [] this->_zombies_army;
}