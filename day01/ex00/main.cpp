//
// Created by Mykhailo Frankevich on 5/3/17.
//

#include "Pony.hpp"

Pony *ponyOnTheHeap()
{
	return (new Pony(10));
}

Pony ponyOnTheStack()
{
	Pony pony(0);

	return (pony);
}

int main(void)
{
	Pony *heap_pony;

	Pony stack_pony = ponyOnTheStack();
	heap_pony = ponyOnTheHeap();

	heap_pony->whereIAm();
	stack_pony.whereIAm();
	
	delete heap_pony;
}
