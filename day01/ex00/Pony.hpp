//
// Created by Mykhailo Frankevich on 4/30/17.
//

#ifndef PISCINE_CPP_PONY_HPP
# define PISCINE_CPP_PONY_HPP

#include <string>
#include <iostream>

class Pony
{
public:
	Pony(int weight);
	~Pony();
	int get_weight() const;
	void set_weight(int _weight);
	Pony operator+(const Pony& b);
	void whereIAm();
private:
	bool _PonyOnHeap();
	int _weight;
};

#endif //PISCINE_CPP_PONY_HPP