//
// Created by Mykhailo Frankevich on 4/30/17.
//

#include "Pony.hpp"

Pony::Pony(int weight) {
	this->set_weight(weight);
}

Pony::~Pony(void) {
}

int Pony::get_weight() const {
	return _weight;
}

void Pony::set_weight(int _weight) {
	Pony::_weight = _weight;
}

void Pony::whereIAm() {
	std::string location;
	location = _PonyOnHeap() ? "heap" : "stack";
	std::cout << "I'm on the " << location << std::endl;
}

Pony Pony::operator+(const Pony& b) {
	Pony pony(0);
	pony.set_weight(this->get_weight() + b.get_weight());
	return pony;
}

bool Pony::_PonyOnHeap()
{
	return ((unsigned long)this < 0x7fff00000000) ? true : false;
}
