//
// Created by Mykhailo Frankevich on 5/7/17.
//

#include "Fixed.hpp"

Fixed::Fixed() {
	this->_value = 0;
	return ;
}

Fixed::Fixed( int value ) {
	this->_value = value * (1 << this->_fraction);
}

Fixed::Fixed( float value ) {
	this->_value = (int)(std::roundf(value * (1 << this->_fraction)));
}

Fixed::Fixed ( const Fixed& old ) {
	*this = old;
	return ;
}

Fixed::~Fixed() {
	return ;
}

void Fixed::operator=( const Fixed& b ) {
	this->_value = (b.getRawBits());
	return ;
}

Fixed Fixed::operator+( const Fixed& rhs ) {
	Fixed obj;

	obj.setRawBits(this->getRawBits() + rhs.getRawBits());
	return (obj);
}

Fixed Fixed::operator-( const Fixed& rhs ) {
	Fixed obj;

	obj.setRawBits(this->getRawBits() - rhs.getRawBits());
	return (obj);
}

Fixed Fixed::operator*( const Fixed& rhs ) {
	Fixed obj;

	obj.setRawBits(( this->getRawBits() * rhs.getRawBits()) >> this->_fraction );
	return (obj);
}

Fixed Fixed::operator/( const Fixed& rhs ) {
	Fixed obj;

	obj.setRawBits(this->getRawBits() / rhs.getRawBits());
	return (obj);
}

Fixed& Fixed::operator++( ) {
	this->_value += 1;
	return *this;
}

Fixed Fixed::operator++( int ) {
	Fixed tmp = *this;
	++(*this);
	return (tmp);
}

Fixed& Fixed::operator--( ) {
	this->_value -= 1;
	return (*this);
}

Fixed Fixed::operator--( int ) {
	Fixed tmp = *this;
	--(*this);
	return (tmp);
}

bool Fixed::operator==(Fixed const & rhs) const
{
	return (this->_value == rhs.getRawBits());
}

bool Fixed::operator!=(Fixed const & rhs) const
{
	return (this->_value != rhs.getRawBits());
}

bool Fixed::operator>(Fixed const & rhs) const
{
	return (this->_value > rhs.getRawBits());
}

bool Fixed::operator>=(Fixed const & rhs) const
{
	return (this->_value >= rhs.getRawBits());
}

bool Fixed::operator<(Fixed const & rhs) const
{
	return (this->_value < rhs.getRawBits());
}

bool Fixed::operator<=(Fixed const & rhs) const
{
	return (this->_value <= rhs.getRawBits());
}

const Fixed Fixed::min( const Fixed& lhs, const Fixed& rhs ) {
	if (lhs < rhs)
		return (lhs);
	else
		return (rhs);
}

const Fixed Fixed::max( const Fixed& lhs, const Fixed& rhs ) {
	if (lhs > rhs)
		return (lhs);
	else
		return (rhs);
}

int Fixed::getRawBits( void ) const {
	return (this->_value);
}

void Fixed::setRawBits( int const raw ) {
	this->_value = raw;
	return ;
}

float Fixed::toFloat( void ) const {
	return ((float)(this->_value / (float)(1 << this->_fraction)));
}

int Fixed::toInt( void ) const {
	return (this->_value >> this->_fraction);
}

std::ostream& operator<<(std::ostream &os, const Fixed &f) {
	os << f.toFloat();
	return os;
}
