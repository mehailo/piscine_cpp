//
// Created by Mykhailo Frankevich on 5/7/17.
//

#ifndef PISCINE_CPP_FIXED_HPP
#define PISCINE_CPP_FIXED_HPP

# include <string>
# include <iostream>
# include <cmath>

class Fixed
{
private:

	int _value;
	static const int _fraction = 8;

public:

	Fixed();
	Fixed( int value );
	Fixed( float value );
	Fixed( const Fixed& );

	~Fixed();

	void operator=(const Fixed& b);
	Fixed operator+(  const Fixed& rhs );
	Fixed operator-( const Fixed& rhs );
	Fixed operator*( const Fixed& rhs );
	Fixed operator/( const Fixed& rhs );

	Fixed& operator++();
	Fixed& operator--();
	Fixed operator++( int );
	Fixed operator--( int );

	bool operator==(Fixed const & rhs) const;
	bool operator!=(Fixed const & rhs) const;
	bool operator>(Fixed const & rhs) const;
	bool operator>=(Fixed const & rhs) const;
	bool operator<(Fixed const & rhs) const;
	bool operator<=(Fixed const & rhs) const;

	static const Fixed min( const Fixed& lhs, const Fixed& rhs );
	static const Fixed max( const Fixed& lhs, const Fixed& rhs );

	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;
};

std::ostream& operator<<(std::ostream &os, const Fixed&f);

#endif //PISCINE_CPP_FIXED_HPP
