//
// Created by Mykhailo Frankevich on 5/7/17.
//

#include "Fixed.hpp"

int main( void )
{
	Fixed a;
	Fixed const b(Fixed(5.05f) * Fixed(2));
	std::cout << "a is                      " << a << std::endl;
	std::cout << "prefix incrementing a     " << ++a << std::endl;
	std::cout << "must be the same as upper " << a << std::endl;
	std::cout << "postfix insrement         " << a++ << std::endl;
	std::cout << "after postfix             " << a << std::endl;
	std::cout << "here is b comes           " << b << std::endl;
	std::cout << "max from a and b is       " << Fixed::max(a, b) << std::endl;
	return 0;
}