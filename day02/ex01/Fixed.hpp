//
// Created by Mykhailo Frankevich on 5/6/17.
//

#ifndef PISCINE_CPP_FIXED_HPP
#define PISCINE_CPP_FIXED_HPP

# include <string>
# include <iostream>
# include <cmath>

class Fixed
{
private:
	int _value;
	static const int _fraction = 8;
public:
	Fixed();
	Fixed( int value );
	Fixed( float value );
	~Fixed();
	Fixed( const Fixed& );

	void operator=(const Fixed& b);

	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;
};

std::ostream& operator<<(std::ostream &os, const Fixed&f);

#endif //PISCINE_CPP_FIXED_HPP
