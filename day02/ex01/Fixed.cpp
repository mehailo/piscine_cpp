//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "Fixed.hpp"

Fixed::Fixed() {
	std::cout << "Default constructor called" << std::endl;
	this->_value = 0;
	return ;
}

Fixed::Fixed( int value ) {
	std::cout << "Int constructor called" << std::endl;
	this->_value = value * 256;
}

Fixed::Fixed( float value ) {
	std::cout << "Float constructor called" << std::endl;
	this->_value = (int)(std::roundf(value * 256));
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
	return ;
}

void Fixed::operator=( const Fixed& b ) {
	std::cout << "Assignation operator called" << std::endl;
	this->setRawBits(b.getRawBits());
	return ;
}

int Fixed::getRawBits( void ) const {
	return (this->_value);
}

void Fixed::setRawBits( int const raw ) {
	this->_value = raw;

	return ;
}

Fixed::Fixed ( const Fixed& old ) {
	std::cout << "Copy constructor called" << std::endl;
	*this = old;
	return ;
}

float Fixed::toFloat( void ) const {
	return ((float)(this->_value / 256.0));
}

int Fixed::toInt( void ) const {
	return (this->_value >> 8);
}

std::ostream& operator<<(std::ostream &os, const Fixed &f) {
	os << f.toFloat();
	return os;
}