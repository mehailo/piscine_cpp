//
// Created by Mykhailo Frankevich on 5/6/17.
//

#include "Fixed.hpp"

Fixed::Fixed() {
	std::cout << "Default constructor called" << std::endl;
	this->_value = 0;
	return ;
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
	return ;
}

void Fixed::operator=( const Fixed& b ) {
	std::cout << "Assignation operator called" << std::endl;
	this->setRawBits(b.getRawBits());
	return ;
}

int Fixed::getRawBits( void ) const {
	std::cout << "getRawBits member function called" << std::endl;
	return (this->_value);
}

void Fixed::setRawBits( int const raw ) {
	this->_value = raw;
	return ;
}

Fixed::Fixed ( const Fixed& old ) {
	std::cout << "Copy constructor called" << std::endl;
	this->_value = old.getRawBits();

	return ;
}