//
// Created by Mykhailo Frankevich on 5/6/17.
//

#ifndef PISCINE_CPP_FIXED_HPP
#define PISCINE_CPP_FIXED_HPP

# include <string>
# include <iostream>

class Fixed
{
private:
	int _value;
	static const int _fraction = 8;
public:
	Fixed();
	~Fixed();
	Fixed( const Fixed& );
	void operator=(const Fixed& b);
	int getRawBits( void ) const;
	void setRawBits( int const raw );
};

#endif //PISCINE_CPP_FIXED_HPP
