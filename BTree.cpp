//
// Created by Mykhel Frankevych on 03.06.17.
//

#include <iostream>
#include <string>
#include <memory>

template <typename T>
struct node {
	node(T value) {
		data = value;
		left = nullptr;
		right = nullptr;
	}
	~node() {
		if (left != nullptr)
			delete left;
		if (right != nullptr)
 			delete right;
	}
	T		data;
	node<T>	*right;
	node<T> *left;
};

template <typename T>
class BTree {
public:

	BTree() {
		_tree = nullptr;
	};
	~BTree() {
		if (_tree != nullptr)
			delete _tree;
	};

	BTree & operator=(BTree const & btree) {};
	BTree(BTree const & btree) {};

	void add(T value) {
		node<T> *elem = new node<T>(value);
		node<T> **crawler = &_tree;
		while (true) {
			if (*crawler == nullptr) {
				*crawler = elem;
				return ;
			}
			else if ((*crawler)->data > value) {
				*crawler = (*crawler)->right;
			}
			else if ((*crawler)->data < value) {
				*crawler = (*crawler)->left;
			}
			else if ((*crawler)->data == value) {
				return ;
			}
		}
	}

	bool find_value(T value) {
		node<T> *crawler = _tree;
		while (true) {
			if (crawler == nullptr) {
				return false;
			}
			else if (crawler->data > value) {
				crawler = crawler->right;
			}
			else if (crawler->data < value) {
				crawler = crawler->left;
			}
			else if (crawler->data == value) {
				return true;
			}
		}
	}

private:

	node<T> *_tree;
};

int main(int argc, char const *argv[])
{
	BTree<int> tree;
	tree.add(5);
	tree.add(6);
	std::cout << tree.find_value(6) << std::endl;
	std::cout << tree.find_value(7) << std::endl;
	tree.add(7);
	std::cout << tree.find_value(7) << std::endl;
	return 0;
}
