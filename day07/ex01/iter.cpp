//
// Created by Mykhailo Frankevich on 5/27/17.
//

#include <string>
#include <iostream>

template <typename T>
void iter(T *array, int len, void (*foo)(T const &)) {
	for (int i = 0; i < len; i++) {
		foo(array[i]);
	}
}

void printInt(int const & i)
{
	std::cout << i << std::endl;
}

void printString(std::string const & str)
{
	std::cout << str << std::endl;
}

int main(void)
{
	int ints[] = {1, 22, 333, 4444, 55555};
	std::string strins[] = {"AA", "BB", "CC", "DD", "EE"};

	std::cout << "Strings: "<< std::endl;
	iter(strins, 5, printString);
	std::cout << std::endl;

	std::cout << "Ints: "<< std::endl;
	iter(ints, 5, printInt);
	std::cout << std::endl;

	return (0);
}
