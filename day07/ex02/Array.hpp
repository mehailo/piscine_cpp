//
// Created by Mykhailo Frankevich on 5/27/17.
//

#include <iostream>
#include <string>

template<typename T>
class Array {
public:
	Array();
	Array(std::size_t n);
	~Array();

	Array(Array const & array);
	Array &operator=(Array const &rhs);

	T &operator[](std::size_t n) const;

	struct OutOfRange : public std::exception
	{
		virtual const char * what() const throw();
	};

	std::size_t getSize() const;

  private:
	T 				*_array;
	std::size_t 	_size;
};

template <typename T>
std::size_t Array<T>::getSize() const
{
	return _size;
}

template <typename T>
Array<T>::Array() {
	return ;
}

template <typename T>
Array<T>::Array(std::size_t n)
{
	_array = new T[n];
	_size = n;
}

template <typename T>
Array<T>::~Array()
{
	delete [] _array;
}

template <typename T>
Array<T>::Array(Array const &array)
{
	*this = array;
}

template <typename T>
Array<T> &Array<T>::operator=(Array<T> const &rhs)
{
	delete [] _array;

	_size = rhs.getSize();
	_array = new T[_size];

	for (int i = 0; i < _size;i++) {
		_array[i] = rhs[i];
	}
	return *this;
}

template <typename T>
const char *Array<T>::OutOfRange::what() const throw()
{
	return "Out of range of the array";
}

template <typename T>
T &Array<T>::operator[](std::size_t n) const{
	try {
		if (n > (_size - 1))
			throw Array::OutOfRange();
		return _array[n];
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		exit(1);
	}
}
