//
// Created by Mykhel Frankevych on 28.05.17.
//

#include "Array.hpp"

int main(void)
{
	try
	{
		Array<int> table(5);

		table[0] = 256;
		table[1] = 512;
		table[2] = 2222;
		table[3] = 333;
		table[4] = 444;
		for (int i = 0; i < 5; i++)
			std::cout << table[i] << std::endl;
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl;


	Array<std::string> table(5);
	table[0] = "Ok";
	table[1] = "Yeah";
	table[2] = "HEY";
	table[3] = "Oh";
	table[4] = "YES";
	for (int i = 0; i < 5; i++)
		std::cout << table[i] << std::endl;

	std::cout << std::endl;
	
	Array<std::string> cp;
	cp = table;
	for (int i = 0; i < 5; i++)
		std::cout << cp[i] << std::endl;
	std::cout << cp[7] << std::endl;

	return (0);
}
