//
// Created by Mykhailo Frankevich on 5/22/17.
//

#include <iomanip>
#include "Convert.hpp"

Convert::Convert()
{
	_value = 0;
	_precision = 1;
	return ;
}

Convert::Convert(char *val)
{
	_value = std::atof(val);
	_precision = _getPrecision(val);

	return ;
}

const char * Convert::Impossible::what() const throw()
{
	return "impossible";
}

const char * Convert::NonDisplayable::what() const throw()
{
	return "Non displayable";
}

Convert::~Convert()
{
	return ;
}

void Convert::operator=(Convert const &rhs)
{
	this->_value = rhs.getRaw_value();
}

Convert::Convert(Convert const &rhs)
{
	*this = rhs;
}

char Convert::toChar()
{
	char result;

	std::cout << "char: ";
	try {
		if (_value < 32 || _value > 126)
			throw Convert::NonDisplayable();
		if (isnan(_value))
			throw Convert::Impossible();
		result = static_cast<char>(_value);
		std::cout << "'" << result << "'" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
		result = 0;
	}
	return result;
}

int Convert::toInt()
{
	int result;

	std::cout << "int: ";
	try {
		if (_value < std::numeric_limits<int>::min() ||
			_value > std::numeric_limits<int>::max() ||
			isnan(_value))
			throw Convert::Impossible();
		result = static_cast<int>(_value);
		std::cout << result << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
		result = 0;
	}
	return result;
}

float Convert::toFloat()
{
	float result;

	std::cout << "float: ";
	result = static_cast<float>(_value);
	std::cout << std::fixed <<std::setprecision(_precision) << result  << "f" << std::endl;
	return result;
}

double Convert::toDouble()
{
	std::cout << "double: ";
	std::cout << std::fixed << std::setprecision(_precision) << _value << std::endl;
	return _value;
}

double Convert::getRaw_value() const
{
	return _value;
}

int Convert::_getPrecision(std::string str)
{
	int result;
	int dot_pos;

	dot_pos = str.find(".");
	if (dot_pos != std::string::npos)
		result = str.length() - dot_pos - 1;
	else
		result = 1;
	return result;
}
