//
// Created by Mykhailo Frankevich on 5/22/17.
//

#ifndef PISCINE_CPP_CONVERT_HPP
#define PISCINE_CPP_CONVERT_HPP


#include <string>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <cmath>

class Convert
{
public:
	Convert();
	Convert(char*);

	~Convert();

	struct Impossible : public std::exception
	{
		virtual const char * what() const throw();
	};
	struct NonDisplayable : public std::exception
	{
		virtual const char * what() const throw();
	};

	void operator=(Convert const & rhs);

	Convert(Convert const & rhs);

	double getRaw_value() const;

	char toChar();
	int toInt();
	float toFloat();
	double toDouble();

private:
	double _value;
	int _precision;

	int _getPrecision(std::string str);
};


#endif //PISCINE_CPP_CONVERT_HPP
