//
// Created by Mykhailo Frankevich on 5/19/17.
//

#include <string>
#include <iostream>
#include "Convert.hpp"

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		std::cout << "There must be one argument" << std::endl;
		return (1);
	}

	Convert convert(argv[1]);

	convert.toChar();
	convert.toInt();
	convert.toFloat();
	convert.toDouble();

	return (0);
}