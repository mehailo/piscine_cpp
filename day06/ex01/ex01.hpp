//
// Created by Mykhailo Frankevich on 5/22/17.
//

#ifndef PISCINE_CPP_EX01_HPP
#define PISCINE_CPP_EX01_HPP

#include <random>
#include <ctime>
#include <limits>
#include <string>
#include <iostream>

struct Data { std::string s1;
	int n; std::string s2; };

void * serialize( void );
Data * deserialize( void * raw );

#endif //PISCINE_CPP_EX01_HPP
