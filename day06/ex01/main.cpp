//
// Created by Mykhailo Frankevich on 5/22/17.
//

#include "ex01.hpp"


int random(int floor, int ceiling)
{
	int randNum = rand()%(ceiling - floor + 1) + floor;
	randNum = randNum % (ceiling - floor) + floor;

	return (randNum);
}

int randomAlphanum()
{

	int randNum;

	do {
		randNum = rand() % 122;
	} while (!isalnum(randNum));
	return (randNum);
}

int main(void) {
	void * herr;
	Data * ne_herr;

	herr = serialize();
	ne_herr = deserialize(herr);

	std::cout << ne_herr->s1 << std::endl;
	std::cout << ne_herr->n << std::endl;
	std::cout << ne_herr->s2 << std::endl;
}

void * serialize( void )
{
	struct Data *a = new struct Data;
	a->s1 = "sdfsdfsd";
	a->s2 = "sdfsdfsd";

	srand(time(nullptr));
	for (int i = 0; i < 8; i++)
	{
		a->s1[i] = static_cast<char>(randomAlphanum());
		a->s2[i] = static_cast<char>(randomAlphanum());
	}
	a->n = random(-123456789, 123456789);
	return(static_cast<void*>(a));
}

Data * deserialize( void * raw )
{
	return (static_cast<Data*>(raw));
}