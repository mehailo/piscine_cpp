//
// Created by Anastasiia Trepyton on 5/17/17.
//

#ifndef NEWCPP_BUREAUCRAT_HPP
#define NEWCPP_BUREAUCRAT_HPP


#include <string>
#include <exception>
#include <ostream>

class Bureaucrat {
public:

	Bureaucrat();

	Bureaucrat(Bureaucrat const & obj);

	Bureaucrat(std::string name, int grade);

	virtual ~Bureaucrat();

	void operator=(Bureaucrat const & obj);

	const std::string &getName() const;
	const int &getGrade() const;

	void incrementGrade();
	void decrementGrade();



	struct GradeTooHighException : public std::exception
	{
		virtual const char * what() const throw();
	};
	struct  GradeTooLowException : public std::exception
	{
		virtual const char * what() const throw();
	};
private:
	std::string name;
	int grade;

};

std::ostream & operator<<(std::ostream &os, const Bureaucrat &bureaucrat);



#endif //NEWCPP_BUREAUCRAT_HPP

